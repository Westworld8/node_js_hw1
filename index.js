const morgan = require('morgan');
const express = require('express');
const app = express();

const appRouter = require('./app/appRouter')

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', appRouter);

app.listen(8080, () => {
    console.log('Server is running');
});