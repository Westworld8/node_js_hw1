const fs = require('fs').promises;
const path = require('path');

const getFiles = async (request, response) => {
    try {
        const isFilesExist = await fs.readdir('./');
        if(!isFilesExist.includes('files')) {
            await fs.mkdir('./files');
        }

        const files = await fs.readdir('./files');
        response.status(200).json({
            message: 'Success',
            files: files
        });
    } catch {
        response.status(500).json({
            message: 'Server error'
        });
    }
};

const getFile = async (request, response) => {
    const filename = request.params.filename;

    const isFilesExist = await fs.readdir('./');
    if(!isFilesExist.includes('files')) {
        await fs.mkdir('./files');
    }

    const files = await fs.readdir('./files');
    if(!files.includes(filename)) {
        return response.status(400).json({
            message: `No file with ${filename} filename found`
        });
    }

    try {
        const birthdate = await fs.stat(`./files/${filename}`);
        const content = await fs.readFile(`./files/${filename}`, 'utf-8')

        response.status(200).json({
            message: 'Success',
            filename: filename,
            content: content,
            extension: path.extname(filename).slice(1),
            uploadedDate: birthdate.birthtime
        });
    } catch {
        response.status(500).json({
            message: 'Server error'
        });
    }
};

const createFile = async (request, response) => {
    try {
        const isFilesExist = await fs.readdir('./');
        if(!isFilesExist.includes('files')) {
            await fs.mkdir('./files');
        }

        const filename = request.body.filename;
        const content = request.body.content;

        await fs.writeFile(`./files/${filename}`, `${content}`, 'utf-8');

        response.status(200).json({
            message: 'File created successfully'
        });
    } catch {
        response.status(500).json({
            message: 'Server error'
        });
    }
};

const deleteFile = async (request, response) => {
    const isFilesExist = await fs.readdir('./');
    if(!isFilesExist.includes('files')) {
        await fs.mkdir('./files');
    }

    const filename = request.params.filename;
    const files = await fs.readdir('./files');

    if(!files.includes(filename)) {
        return response.status(400).json({
            message: `No file with ${filename} filename found`
        });
    }

    try {
        await fs.unlink(`./files/${filename}`);

        response.status(200).json({
            message: 'File was successfully deleted',
        });
    } catch {
        response.status(500).json({
            message: 'Server error'
        });
    }
};

module.exports = {getFiles, getFile, createFile, deleteFile};