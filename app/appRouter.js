const express = require('express');
const router = express.Router();

const {checkPostRequest} = require('./middleware');
const {getFile, getFiles, createFile, deleteFile} = require('./functions');

router.get('/', getFiles);
router.get('/:filename', getFile);
router.post('/', checkPostRequest, createFile);
router.delete('/:filename', deleteFile);

module.exports = router;