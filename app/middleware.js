const path = require('path');

const checkPostRequest =  async (request, response, next) => {
    const filename = request.body.filename;
    const content = request.body.content;

    if(!filename) {
        return response.status(400).json({
            message: `Please specify 'filename' parameter`
        });
    }

    const file_ext = path.extname(filename);
    if(!content) {
        return response.status(400).json({
            message: `Please specify 'content' parameter`
        });
    }

    const allowed_ext = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
    if(!allowed_ext.includes(file_ext)) {
        return response.status(400).json({
            message: `Not supported file extension`
        });
    }

    next();
};

module.exports = {checkPostRequest};